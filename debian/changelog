libical3 (3.0.20-1) unstable; urgency=medium

  * New upstream release
  * d/patches: Remove patch to fix build on hppa and powerpc,
    applied updtream

 -- Nicolas Mora <babelouest@debian.org>  Mon, 10 Mar 2025 20:54:05 -0400

libical3 (3.0.19-4) unstable; urgency=medium

  [Nicolas Mora]
  * d/control: remove builddep gobject-introspection-bin
    (Closes: #1094623)

  [Simon McVittie]
  * d/control: Improve GObject-Introspection handling

 -- Nicolas Mora <babelouest@debian.org>  Sat, 01 Feb 2025 11:36:57 -0500

libical3 (3.0.19-3) unstable; urgency=medium

  * Upload to unstable (Closes: #1091461)

 -- Nicolas Mora <babelouest@debian.org>  Wed, 08 Jan 2025 16:30:21 -0500

libical3 (3.0.19-2) experimental; urgency=medium

  * d/patches: Add patch to fix build on hppa and powerpc

 -- Nicolas Mora <babelouest@debian.org>  Mon, 06 Jan 2025 11:13:23 -0500

libical3 (3.0.19-1) unstable; urgency=medium

  * New upstream release
  * d/lintian-overrides: ignore package-name-doesnt-match-sonames libical3

 -- Nicolas Mora <babelouest@debian.org>  Wed, 25 Dec 2024 11:18:53 -0500

libical3 (3.0.18-1) unstable; urgency=medium

  [Helmut Grohne]
  * Improve cross building: Add a native pass for the ical glib generator.
    (Closes: #1061068)

  [Nicolas Mora]
  * New upstream release
  * CI: change gitlab-ci script to salsa-ci script
  * d/copyright: Update copyright year
  * d/control: upgrade standards version to 4.7.0

 -- Nicolas Mora <babelouest@debian.org>  Thu, 25 Apr 2024 07:12:01 -0400

libical3 (3.0.17-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062372

 -- Benjamin Drung <bdrung@debian.org>  Wed, 28 Feb 2024 12:49:22 +0000

libical3 (3.0.17-1) unstable; urgency=medium

  * New Upstream release
  * d/control: Update Standards-Version to 4.6.2 (no change)
  * d/libical3.symbols: Update symbols
  * d/.gitlab-ci.yml : Add salsa CI script

 -- Nicolas Mora <babelouest@debian.org>  Sat, 14 Oct 2023 13:08:52 -0400

libical3 (3.0.16-1) unstable; urgency=medium

  * New Upstream release (Closes: #1021698)
  * d/patches: remove test-regression patch

 -- Nicolas Mora <babelouest@debian.org>  Tue, 18 Oct 2022 07:32:48 -0400

libical3 (3.0.15-2) unstable; urgency=medium

  * Workaround regression test for 32 bits architectures

 -- Nicolas Mora <babelouest@debian.org>  Mon, 10 Oct 2022 08:48:17 -0400

libical3 (3.0.15-1) unstable; urgency=medium

  * New Upstream release
  * d/control: Update Standards-Version to 4.6.1 (no change)
  * d/libical3.symbols: Update symbols

 -- Nicolas Mora <babelouest@debian.org>  Sun, 09 Oct 2022 10:53:18 -0400

libical3 (3.0.14-1) unstable; urgency=medium

  * New Upstream release

 -- Nicolas Mora <babelouest@debian.org>  Sat, 05 Feb 2022 13:31:59 -0500

libical3 (3.0.13-1) unstable; urgency=medium

  * New Upstream release
  * d/copyright: Update copyright year to 2022
  * d/docs: replace ReadMe.txt with README.md
  * symbols: Add missing Build-Depends-Package and missing symbol

 -- Nicolas Mora <babelouest@debian.org>  Mon, 17 Jan 2022 19:51:30 -0500

libical3 (3.0.12-1) unstable; urgency=medium

  * New Upstream release
  * d/tests: Fix autopkgtest to use package files

 -- Nicolas Mora <babelouest@debian.org>  Sun, 12 Dec 2021 19:53:32 -0500

libical3 (3.0.11-2) unstable; urgency=medium

  * libical-dev: Restore cmake config files (Closes: #996474)

 -- Nicolas Mora <babelouest@debian.org>  Thu, 21 Oct 2021 17:12:58 -0400

libical3 (3.0.11-1) unstable; urgency=medium

  * New upstream release
  * d/control: Update Standards-Version to 4.6.0 (no change)

 -- Nicolas Mora <babelouest@debian.org>  Wed, 20 Oct 2021 08:20:00 -0400

libical3 (3.0.10-1) unstable; urgency=medium

  * New upstream version

 -- Nicolas Mora <babelouest@debian.org>  Sat, 28 Aug 2021 11:25:35 -0400

libical3 (3.0.9-2) unstable; urgency=medium

  [Helmut Grohne]
  * Annotate build dependency python3-gi <!nocheck>. (Closes: #981207)

 -- Nicolas Mora <babelouest@debian.org>  Wed, 03 Feb 2021 15:05:18 -0500

libical3 (3.0.9-1) unstable; urgency=medium

  [Debian Janitor]
  * Trim trailing whitespace.
  * Set debhelper-compat version in Build-Depends.
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Drop transition for old debug package migration.

  [Nicolas Mora]
  * New upstream version
  * Bump debhelper from old 11 to 13.
  * d/control: add Rules-Requires-Root
  * d/control: remove builddep dh-exec
  * d/control: Update Standards-Version to 4.5.1 (no change)

 -- Nicolas Mora <babelouest@debian.org>  Sat, 16 Jan 2021 17:54:51 -0500

libical3 (3.0.8-1) unstable; urgency=medium

  * New upstream version

 -- Nicolas Mora <nicolas@babelouest.org>  Fri, 13 Mar 2020 08:20:27 -0400

libical3 (3.0.7-2) unstable; urgency=medium

  * Fix .typelib install path after .install files were converted to dh-exec.
    Thanks Iain Lane <laney@debian.org>! (Closes: #951044)

 -- Nicolas Mora <nicolas@babelouest.org>  Tue, 11 Feb 2020 07:11:25 -0500

libical3 (3.0.7-1) unstable; urgency=medium

  * New upstream version. (Closes: #950461)
  * Update Standards-Version to 4.5.0 (No change)

 -- Nicolas Mora <nicolas@babelouest.org>  Sun, 02 Feb 2020 09:14:47 -0500

libical3 (3.0.5-2) unstable; urgency=medium

  * Update Standards-Version to 4.4.0 (No change)

 -- Nicolas Mora <nicolas@babelouest.org>  Sat, 31 Aug 2019 13:17:08 -0400


libical3 (3.0.5-1) unstable; urgency=medium

  * New upstream version. (Closes: #929126)
  * debian/control: Add Multi-Arch: foreign to package libical-doc

 -- Nicolas Mora <nicolas@babelouest.org>  Sat, 25 May 2019 10:17:19 -0400

libical3 (3.0.4-3) unstable; urgency=medium

  * debian/control: Add VCS browser and VCS GIT
  * debian/libical3.symbols: Make a symbol optional
  * debian/control: Remove autopkgtest from build dep (Closes: #918998)

 -- Nicolas Mora <nicolas@babelouest.org>  Fri, 11 Jan 2019 12:29:23 -0500

libical3 (3.0.4-2) unstable; urgency=medium

  * Fix Enable GObject Introspection
  * Add debian/tests suite with autopkgtests
  * Update Standards-Version to 4.3.0 (No change)
  * debian/copyright: Full rewrite to match debian standards

 -- Nicolas Mora <nicolas@babelouest.org>  Sun, 06 Jan 2019 13:38:06 -0500

libical3 (3.0.4-1) unstable; urgency=medium

  * New upstream version.
  * Enable GObject Introspection and Vala .vapi file generation. Closes: #909296

 -- Nicolas Mora <nicolas@babelouest.org>  Sat, 22 Sep 2018 10:14:38 -0400

libical3 (3.0.3-1) unstable; urgency=medium

  * New upstream version.
  * New maintainer (Closes: #884073)

 -- Nicolas Mora <nicolas@babelouest.org>  Thu, 19 Jul 2018 21:41:56 -0400

libical3 (3.0.1-5) unstable; urgency=medium

  * QA upload.
  * Don't run the testsuite on kfreebsd-*, hanging on the buildds.

 -- Matthias Klose <doko@debian.org>  Sun, 10 Dec 2017 09:32:52 +0100

libical3 (3.0.1-4) unstable; urgency=medium

  * QA upload.
  * Fix white space typo in the rules file.

 -- Matthias Klose <doko@debian.org>  Sat, 09 Dec 2017 19:07:15 +0100

libical3 (3.0.1-3) unstable; urgency=medium

  * QA upload.
  * Rename libical3-dev to libical-dev, now that libical only builds
    libical2-dev.
  * Drop the M-A: same attribute for libical-dev again, not M-A: ready.
  * Ignore test results on kfreebsd, failing on the buildds.

 -- Matthias Klose <doko@debian.org>  Sat, 09 Dec 2017 14:28:26 +0100

libical3 (3.0.1-2) unstable; urgency=medium

  * QA upload.
  * libical3-dev: Provide libical-dev.
  * Bump standards version.

 -- Matthias Klose <doko@debian.org>  Fri, 08 Dec 2017 14:56:42 +0100

libical3 (3.0.1-1) unstable; urgency=medium

  * QA upload.
  * New upstream version.
    - Package as a new source, the API is incompatible to libical2 and not
      many packages are ready to migrate to the new API.
    - Fix CVE-2016-9584: heap use-after-free.
  * Remove patches applied upstream.
  * Update debian/copyright to MPL 2.0, add additional copyright holders.
  * Bump the libical soname.
  * Build-depend on libdb-dev and libxml2-dev.

 -- Matthias Klose <doko@debian.org>  Mon, 04 Dec 2017 09:47:57 +0100

libical (2.0.0-1) unstable; urgency=medium

  * QA upload.
  * Set the maintainer to the Debian QA group. See #879395.
  * Bump the standards version.
  * Update homepage. Closes: #873555.
  * Make the build reproducible (Chris Lamb). Closes: #796360.

 -- Matthias Klose <doko@debian.org>  Tue, 07 Nov 2017 20:07:30 +0100

libical (2.0.0-0.5) unstable; urgency=medium

  * Non-maintainer upload.
  * Run debian/libical2.symbols through c++filt and tag all c++ symbols
  * Mark std::vector::_M_insert_aux as optional
  * Add std::vector::_M_emplace_back_aux and mark it as optional.
    (Closes: #825477)

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 01 Aug 2016 13:04:19 +0200

libical (2.0.0-0.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Upload to unstable.

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 10 May 2016 13:54:26 +0200

libical (2.0.0-0.3) experimental; urgency=medium

  * Non-maintainer upload.
  * Drop obsolete files in debian/:
    - debian/.svnignore
    - debian/libical1a.docs (see debian/docs)
    - debian/libical1a.install (see debian/libical2.install)
    - debian/libical1a.symbols (see debian/libical2.symbols)
    - debian/python-libical.install
  * Blind attempts at arm64,ppc64el,mips64el buildd/testsuite fails
    which aren't reproducible on porterboxes:
    - debian/rules: switch --parallel to --no-parallel.
      (note how icalrecur-r/icalrecur tests both output test.out)

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 06 May 2016 05:29:33 +0200

libical (2.0.0-0.2) experimental; urgency=medium

  * Non-maintainer upload.
  * Add build-dependency on libicu-dev to enable RSCALE support.
    - also make libical-dev depend on libicu-dev
  * Cherry-pick patches from upstreams 2.0 git branch:
    - 0001-icallangbind.h-fix-typo-on-the-icallangbind_quote_as.patch
    - 0002-icalrecur.c-fix-compile-on-ARM.patch

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 28 Apr 2016 20:14:56 +0200

libical (2.0.0-0.1) experimental; urgency=medium

  * Non-maintainer upload.
  * New upstream release
    - includes reproducible build fix (Closes: #796360)
  * Rename libical1a -> libical2 to match new so name. (Closes: #797003)
  * Drop libical-dbg and rely on automatic dbgsym.
    - add dbgsym-migration flags and dump dh build-dep version.
  * Bump Standards-Version to 3.9.8
  * Multi-archify the packages (Closes: #813017)
    Thanks to Matthias Klose for the original patch.

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 19 Apr 2016 16:19:47 +0200

libical (1.0.1-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Import debian/watch as suggested by packages.qa.debian.org
  * New upstream release
    - includes commit 60ebe54f 'icaltime.c: fix typo' (Closes: #791438)
    - includes fixes to icaltimezone (Closes: #760430)
  * Drop both patches, now included upstream.
    - debian/patches/fix_timezone_crash.patch,
    - debian/patches/reproducible-generator.patch
  * Update debian/*docs
    - Don't ship now non-existant NEWS
    - Spell README as ReadMe.txt
    - Add THANKS and ReleaseNotes.txt
  * Update debian/libical1a.symbols

 -- Andreas Henriksson <andreas@fatal.se>  Wed, 19 Aug 2015 19:27:40 +0200

libical (1.0-1.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libical1 to libical1a because the change introduced in the previous
    upload breaks compatibility. (Closes: #774242)

 -- Ivo De Decker <ivodd@debian.org>  Sat, 03 Jan 2015 14:58:46 +0100

libical (1.0-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Sort keys to generate reproducible source code. (Closes: #773916)

 -- Dimitri John Ledkov <dimitri.j.ledkov@linux.intel.com>  Mon, 29 Dec 2014 18:42:22 +0000

libical (1.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add missing license information to debian/copyright.
    (Closes: #738967)

 -- Tobias Hansen <thansen@debian.org>  Sun, 19 Oct 2014 00:28:41 +0200

libical (1.0-1) unstable; urgency=medium

  * New upstream release (Closes: #717417)
  * Remove patches - merged upstream:
    - 0001_convert_float_to_strings_with_snprintf_sfbug3493034.patch
    - fix_arm_ftbfs.patch
    - fix_ftbfs_dependencies.patch
  * Update debian/control:
    - bump soname (libical0 -> libical1)
    - bump Standards-Version to 3.9.5 (no changes needed)

 -- Fathi Boudra <fabo@debian.org>  Sun, 09 Feb 2014 12:05:44 +0200

libical (0.48-2) unstable; urgency=low

  * Add patches (Thanks to Robie Basak):
    - fix_arm_ftbfs.patch: fix FTBFS on ARM: do not unput when input returns
      EOF. (Closes: #663624, #670138)
    - fix_ftbfs_dependencies.patch: fix CMakeLists.txt dependencies.
      (Closes: #670139)
    - fix_timezone_crash.patch: workaround to avoid heap corruption until
      upstream have a complete fix. (Closes: #670140)

 -- Fathi Boudra <fabo@debian.org>  Sun, 13 May 2012 12:52:58 +0300

libical (0.48-1) unstable; urgency=low

  * New upstream release. (Closes: #638597)
  * Add debug libical debug package.
  * Drop patches:
    - 01_fix_headers.diff - merged upstream.
    - 02_fix_bswap32_ftbfs_kfreebsd.diff - merged upstream.
  * Add 0001_convert_float_to_strings_with_snprintf_sfbug3493034.patch
    using snprintf to convert float to strings isn't locale aware.

 -- Fathi Boudra <fabo@debian.org>  Sun, 11 Mar 2012 09:21:07 +0200

libical (0.44-3) unstable; urgency=low

  * Add patch to fix FTBFS on GNU/kFreeBSD due to an undefined reference to
    __bswap32. Thanks to Petr Salinger. (Closes: #566776)

 -- Fathi Boudra <fabo@debian.org>  Mon, 25 Jan 2010 11:22:20 +0100

libical (0.44-2) unstable; urgency=low

  * Add patch to fix headers inclusion (Closes: #549757)
  * Switch to dpkg-source 3.0 (quilt) format

 -- Fathi Boudra <fabo@debian.org>  Sat, 16 Jan 2010 23:41:47 +0100

libical (0.44-1) unstable; urgency=low

  * New upstream release
  * Remove all patches - merged upstream
  * Update debian/control:
    - Drop cdbs and quilt build dependencies
    - Bump Standards-Version to 3.8.3 (no changes needed)
  * Update debian/copyright: debian packaging copyright
  * Update debian/libical0.symbols: new symbols added (and 2 missing)
  * Update debian/rules: switch to dh usage
  * Update debian/source.lintian-overrides

 -- Fathi Boudra <fabo@debian.org>  Sat, 03 Oct 2009 09:56:13 +0200

libical (0.43-3) unstable; urgency=low

  * Set ICAL_ERRORS_ARE_FATAL to false by default (Closes: #537346)

 -- Fathi Boudra <fabo@debian.org>  Fri, 17 Jul 2009 14:34:45 +0200

libical (0.43-2) unstable; urgency=low

  * Add 03_fix_implicit_pointer_conversion_bug511598.diff (Closes: #511598)
    It fixed functions implicitly converted to pointer
    Thanks to Dann Frazier

 -- Fathi Boudra <fabo@debian.org>  Mon, 12 Jan 2009 18:39:36 +0100

libical (0.43-1) unstable; urgency=low

  * New upstream release
  * Update debian/libical0.symbols:
    * Add ical_tzid_prefix and icaltimezone_set_tzid_prefix

 -- Fathi Boudra <fabo@debian.org>  Mon, 12 Jan 2009 10:57:53 +0100

libical (0.42-1) unstable; urgency=low

  * New upstream release (Closes: #503960)
    Thanks to Yavor Doganov for the GNU/kFreeBSD patch
  * Switch from autotools to cmake build system
  * Update build dependencies:
    * Add quilt
    * Remove chrpath
    * Replace autotools-dev by cmake
  * Bump compat/debhelper to 7
  * Fix lintian warnings:
    * Add debian/source.lintian-overrides to override outdated
      config.guess and config.sub
    * Fix debhelper-but-no-misc-depends
  * Write a new debian/rules as we use cmake now
  * Add debian/libical0.symbols file
  * Add patches:
    * 01_add_static_libraries.diff
      Build static libraries when cmake build system is used
    * 02_fix_ical.h_install_path.diff
      Install /usr/include/ical.h to inform the path is deprecated and
      /usr/include/libical.h should be used

 -- Fathi Boudra <fabo@debian.org>  Fri, 09 Jan 2009 19:07:45 +0100

libical (0.33-1) unstable; urgency=low

  * New upstream release
  * Update libical-dev package: add libical.pc

 -- Fathi Boudra <fabo@debian.org>  Mon, 15 Sep 2008 09:50:36 +0200

libical (0.32-1) unstable; urgency=low

  * New upstream release
  * Use system time zone data:
    * Add tzdata build-dependency and dependency on libical0
  * Bump Standards-Version to 3.8.0 (no changes needed)

 -- Fathi Boudra <fabo@debian.org>  Sun, 07 Sep 2008 12:35:48 +0200

libical (0.31-1) unstable; urgency=low

  * New upstream release
  * Add build dependency on chrpath to clean up rpath

 -- Fathi Boudra <fabo@debian.org>  Thu, 13 Mar 2008 20:13:59 +0100

libical (0.30-1) unstable; urgency=low

  * New upstream release. (Closes: #454172)
  * Bump compat/debhelper to 6.
  * Update my e-mail address.
  * Update Homepage field.
  * Update copyrights.

 -- Fathi Boudra <fabo@debian.org>  Sat, 02 Feb 2008 17:50:37 +0100

libical (0.27-1) unstable; urgency=low

  * Initial Debian release (Closes: #404862)
  * Remove dirs and README.Debian
  * Add soname patch
  * compat: bumped to 5
  * control:
    * convert package to cdbs
    * add myself as uploader
    * bump Standards-Version to 3.7.2
    * rewrite descriptions
  * copyright: complete rewrite

  [ Wilfried Goesgens ]
  * Update to aurore ical.

 -- Fathi Boudra <fboudra@free.fr>  Sun, 25 Nov 2007 21:14:55 +0100
